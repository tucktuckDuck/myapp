package com.miniproject.bookshelf.repository;

import com.miniproject.bookshelf.entities.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<BookEntity, Long> {
    public List<BookEntity> findBookByWriter(String writer);
}