package com.miniproject.bookshelf.repository;

import com.miniproject.bookshelf.entities.CatagoryIdEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CatagoryIdRepository extends MongoRepository<CatagoryIdEntity, Long> {


}
