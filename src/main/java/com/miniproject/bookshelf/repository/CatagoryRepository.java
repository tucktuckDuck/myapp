package com.miniproject.bookshelf.repository;

import com.miniproject.bookshelf.entities.CatagoryEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CatagoryRepository extends MongoRepository<CatagoryEntity, Long> {

    public CatagoryEntity findByName(String name);

    public List<CatagoryEntity> findCatagoryByWriter(String writer);

}
