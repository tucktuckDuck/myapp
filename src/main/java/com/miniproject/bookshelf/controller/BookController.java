package com.miniproject.bookshelf.controller;

import com.miniproject.bookshelf.entities.BookEntity;
import com.miniproject.bookshelf.model.BookModel;
import com.miniproject.bookshelf.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/getbook")
    public List<BookModel> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GetMapping("/getbook/{id}")
    public BookModel getBookById(@PathVariable Long id) {
        return bookService.getBookById(id);
    }

    @PostMapping("/addbook")
    public BookEntity addBook(@RequestBody BookEntity book) {
        bookService.addBook(book);
        return book;
    }

    @PutMapping("/updatebook/{id}")
    public BookModel updateBook(@PathVariable Long id, @RequestBody BookEntity updatedBook) {
        return bookService.updateBook(id, updatedBook);
    }

    @DeleteMapping("/deletebook/{id}")
    public void deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
    }
}
