package com.miniproject.bookshelf.controller;

import com.miniproject.bookshelf.entities.CatagoryEntity;
import com.miniproject.bookshelf.model.CatagoryModel;
import com.miniproject.bookshelf.service.CatagoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books")
public class CatagoryController {

    @Autowired
    private CatagoryService catagoryService;

    @GetMapping("/getcatagory")
    public List<CatagoryModel> getAllCatagory() {
        return catagoryService.getAllCatagories();
    }

    @GetMapping("/getcatagory/{id}")
    public CatagoryModel getCatagoryById(@PathVariable Long id) {
        return catagoryService.getCatagoryById(id);
    }

    @PostMapping("/addcatagory")
    public void addCatagory(@RequestBody CatagoryEntity catagory) {
        catagoryService.addCatagory(catagory);
    }

    @PutMapping("/updatecatagory/{id}")
    public void updateCatagory(@PathVariable Long id, @RequestBody CatagoryEntity updatedCatagory) {
        catagoryService.updateCatagory(id, updatedCatagory);
    }

    @DeleteMapping("/deletecatagory/{id}")
    public void deleteCatagory(@PathVariable Long id) {
        catagoryService.deleteCatagory(id);
    }
}
