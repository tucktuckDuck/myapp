package com.miniproject.bookshelf.service;

import com.miniproject.bookshelf.entities.BookEntity;
import com.miniproject.bookshelf.entities.CatagoryEntity;
import com.miniproject.bookshelf.entities.CatagoryIdEntity;
import com.miniproject.bookshelf.model.CatagoryModel;
import com.miniproject.bookshelf.repository.BookRepository;
import com.miniproject.bookshelf.repository.CatagoryIdRepository;
import com.miniproject.bookshelf.repository.CatagoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CatagoryImp implements CatagoryService {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CatagoryRepository catagoryRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private CatagoryIdRepository catagoryIdRepository;


    public List<CatagoryModel> getAllCatagories() {

        List<CatagoryEntity> listCatagory = catagoryRepository.findAll();
        List<CatagoryModel> listCatagoryResult = new ArrayList<>();
        for (CatagoryEntity catagory : listCatagory) {
            CatagoryModel tempCatagory = new CatagoryModel();
            List<BookEntity> bookFromWriter = bookRepository.findBookByWriter(catagory.getWriter());
            tempCatagory.setBook(bookFromWriter);
            tempCatagory.setId(catagory.getId());
            tempCatagory.setName(catagory.getName());
            tempCatagory.setWriter(catagory.getWriter());
            tempCatagory.setDescription(catagory.getDescription());

            listCatagoryResult.add(tempCatagory);
        }
        return listCatagoryResult;
    }

    public CatagoryModel getCatagoryById(Long id) {
        CatagoryEntity catagoryEntity = catagoryRepository.findById(id).orElse(null);
        CatagoryModel catagoryResult = new CatagoryModel();
        catagoryResult.setBook(bookRepository.findBookByWriter(catagoryEntity.getWriter()));
        catagoryResult.setName(catagoryEntity.getName());
        catagoryResult.setDescription(catagoryEntity.getDescription());
        catagoryResult.setWriter(catagoryEntity.getWriter());
        return catagoryResult;
    }

    public void addCatagory(CatagoryEntity catagory) {
        CatagoryIdEntity catagoryId = catagoryIdRepository.findAll().get(0);
        Query query = new Query().addCriteria(Criteria.where("staticId").is(catagoryId.getStaticId()));
        System.out.println(catagoryId.getStaticId());
        long newId = catagoryId.getStaticId() + (long) 1;
        catagory.setId(newId);
        catagoryRepository.save(catagory);
        Update update = new Update().set("staticId", newId);
        mongoTemplate.upsert(query, update, CatagoryIdEntity.class);
//        catagoryId.setStaticId(newId);
//        catagoryIdRepository.save(catagoryId);
    }

    public static long generateNewId(MongoTemplate mongoTemplate) {
        // Query to find the maximum id value
        Query query = new Query().with(Sort.by(Sort.Order.desc("id"))).limit(1);

        // Execute the query to get the maximum id
        CatagoryEntity maxIdEntity = mongoTemplate.findOne(query, CatagoryEntity.class);

        // Calculate the new id
        long newId = (maxIdEntity != null) ? maxIdEntity.getId() + 1 : 0;

        return newId;
    }

    public void updateCatagory(Long id, CatagoryEntity updatedCatagory) {
        CatagoryEntity existingCatagory = catagoryRepository.findById(id).orElse(null);
        Query query = new Query().addCriteria(Criteria.where("id").is(id));
        if (existingCatagory != null) {
            Update update = new Update().set("id", updatedCatagory.getId());
            update.set("writer", updatedCatagory.getWriter());
            update.set("name", updatedCatagory.getName());
            update.set("description", updatedCatagory.getDescription());
            mongoTemplate.upsert(query, update, CatagoryIdEntity.class);
        }
    }

    public void deleteCatagory(Long id) {
        catagoryRepository.deleteById(id);
    }
}
