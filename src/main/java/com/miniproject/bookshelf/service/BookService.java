package com.miniproject.bookshelf.service;

import com.miniproject.bookshelf.entities.BookEntity;
import com.miniproject.bookshelf.model.BookModel;

import java.util.List;

public interface BookService {
    public List<BookModel> getAllBooks();

    public BookModel getBookById(Long id);

    public BookEntity addBook(BookEntity book);

    public BookModel updateBook(Long id, BookEntity updatedBook);

    public void deleteBook(Long id);
}
