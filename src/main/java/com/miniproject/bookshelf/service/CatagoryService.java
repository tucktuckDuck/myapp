package com.miniproject.bookshelf.service;

import com.miniproject.bookshelf.entities.CatagoryEntity;
import com.miniproject.bookshelf.model.CatagoryModel;

import java.util.List;

public interface CatagoryService {
    public List<CatagoryModel> getAllCatagories();

    public CatagoryModel getCatagoryById(Long id);

    public void addCatagory(CatagoryEntity catagory);

    public void updateCatagory(Long id, CatagoryEntity updatedCatagory);

    public void deleteCatagory(Long id);
}
