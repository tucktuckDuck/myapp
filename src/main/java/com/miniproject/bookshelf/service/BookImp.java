package com.miniproject.bookshelf.service;

import com.miniproject.bookshelf.entities.BookEntity;
import com.miniproject.bookshelf.entities.CatagoryEntity;
import com.miniproject.bookshelf.model.BookModel;
import com.miniproject.bookshelf.repository.BookRepository;

import java.util.ArrayList;
import java.util.List;

import com.miniproject.bookshelf.repository.CatagoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BookImp implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CatagoryRepository catagoryRepository;

    public List<BookModel> getAllBooks() {
        List<BookEntity> listBook = bookRepository.findAll();
        List<BookModel> listBookResult = new ArrayList<>();
        for (BookEntity book : listBook) {
            BookModel tempBook = new BookModel();
            List<CatagoryEntity> catagoryFromWriter = catagoryRepository.findCatagoryByWriter(book.getWriter());
            tempBook.setCatagories(catagoryFromWriter);
            tempBook.setId(book.getId());
            tempBook.setTitle(book.getTitle());
            tempBook.setDescription(book.getDescription());
            tempBook.setPublished(book.isPublished());
            tempBook.setWriter(book.getWriter());
            listBookResult.add(tempBook);
        }
        return listBookResult;
    }

    public BookModel getBookById(Long id) {
        BookEntity bookEntity = bookRepository.findById(id).orElse(null);
        BookModel bookResult = new BookModel();
        bookResult.setCatagories(catagoryRepository.findCatagoryByWriter(bookEntity.getWriter()));
        bookResult.setPublished(bookEntity.isPublished());
        bookResult.setId(bookEntity.getId());
        bookResult.setTitle(bookEntity.getTitle());
        bookResult.setDescription(bookEntity.getDescription());
        bookResult.setWriter(bookEntity.getWriter());
        return bookResult;
    }

    public BookEntity addBook(BookEntity book) {
        bookRepository.save(book);
        return book;
    }

    public BookModel updateBook(Long id, BookEntity updatedBook) {
        BookEntity existingBook = bookRepository.findById(id).orElse(null);
        if (existingBook != null) {
            existingBook.setTitle(updatedBook.getTitle());
            existingBook.setDescription(updatedBook.getDescription());
            existingBook.setPublished(updatedBook.isPublished());
            bookRepository.save(existingBook);
        }
        BookModel bookResult = new BookModel();
        bookResult.setCatagories(catagoryRepository.findCatagoryByWriter(existingBook.getWriter()));
        bookResult.setPublished(existingBook.isPublished());
        bookResult.setId(existingBook.getId());
        bookResult.setTitle(existingBook.getTitle());
        bookResult.setDescription(existingBook.getDescription());
        bookResult.setWriter(existingBook.getWriter());
        return bookResult;
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

}