package com.miniproject.bookshelf.entities;

import jakarta.persistence.Id;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "catagoryId")
@Data
public class CatagoryIdEntity {

    private long staticId;

}