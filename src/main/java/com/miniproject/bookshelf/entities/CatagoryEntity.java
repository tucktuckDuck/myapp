package com.miniproject.bookshelf.entities;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "catagory")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CatagoryEntity {

    @Id
    private long id;

    private String name;

    private String description;

    private String writer;

    @Override
    public String toString() {
        return "Book [id=" + id + ", title=" + name + ", desc=" + description + "]";
    }
}