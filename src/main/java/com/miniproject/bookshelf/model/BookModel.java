package com.miniproject.bookshelf.model;

import com.miniproject.bookshelf.entities.CatagoryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookModel {
    private long id;
    private String title;
    private String description;
    private boolean published;
    private String writer;
    private List<CatagoryEntity> catagories;
}
