package com.miniproject.bookshelf.model;

import com.miniproject.bookshelf.entities.BookEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatagoryModel {
    private Long id;
    private String name;
    private String description;
    private String writer;
    private List<BookEntity> book;
}
