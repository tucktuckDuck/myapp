package com.miniproject.bookshelf.controller;

import com.miniproject.bookshelf.controller.BookController;
import com.miniproject.bookshelf.entities.BookEntity;
import com.miniproject.bookshelf.model.BookModel;
import com.miniproject.bookshelf.repository.BookRepository;
import com.miniproject.bookshelf.service.BookImp;
import com.miniproject.bookshelf.service.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

//class FooServiceTest {
//    @Mock
//    private FooRepository fooRepository;
//
//    @InjectMocks
//    private FooService fooService;
//
//    @BeforeEach
//    void setUp() {
//        when(fooRepository.get()).thenReturn("Hello Mocked");
//    }
//
//    @Test
//    @DisplayName("Mock output of the BarService using mockito")
//    void barTest() {
//        assertEquals("Hello Mocked", fooService.bar());
//    }
//}
@ExtendWith(MockitoExtension.class)
@DisplayName("Demo Spring Boot with JUnit 5 + Mockito")
class BookControllerTest {

    @InjectMocks
    BookController bookController;
    @Mock
    BookService bookService;

    @Mock
    BookImp bookImp;

    @Mock
    BookRepository bookRepository;

    BookModel bookModel;

    @BeforeEach
    void setUp() {
        lenient().when(bookService.getAllBooks()).thenReturn(Collections.singletonList(new BookModel()));
    }

    @Test
    void getAllBooks() {
        List<BookModel> bookList = new ArrayList<>();
        BookModel book1 = new BookModel(1, "Jack Sparrow", "Test desc", true, "John", null);
        BookModel book2 = new BookModel(2, "Peterpan", "Test desc2", true, "Peter", null);
        bookList.add(book1);
        bookList.add(book2);
        lenient().when(bookService.getAllBooks()).thenReturn(bookList);
        List<BookModel> result = bookController.getAllBooks();
        assertEquals(result.size(), 2);
        assertEquals(result, bookList);

        List<BookEntity> bookEntity = new ArrayList<>();
        BookEntity book3 = new BookEntity(1, "Jack Sparrow", "Test desc", true, "John");
        BookEntity book4 = new BookEntity(2, "Peterpan", "Test desc2", true, "Peter");
        bookEntity.add(book3);
        bookEntity.add(book4);
        lenient().when(bookRepository.findAll()).thenReturn(bookEntity);
        List<BookEntity> books = bookRepository.findAll();
//        System.out.println(books);
        assertEquals(bookEntity, books);
        verify(bookRepository).findAll();

//        assertThat(result.getEmployeeList().size()).isEqualTo(2);
//        assertThat(result.getEmployeeList().get(0).getFirstName()).isEqualTo(employee1.getFirstName());
//        assertThat(result.getEmployeeList().get(1).getFirstName()).isEqualTo(employee2.getFirstName());
    }

    @Test
    void getBookById() {
        BookModel book1 = new BookModel(1, "Jack Sparrow", "Test desc", true, "John", null);
//        BookModel book2 = new BookModel(2, "Peterpan", "Test desc2", true, "Peter", null);
        lenient().when(bookService.getBookById(1L)).thenReturn(book1);
        BookModel result = bookController.getBookById(1L);
        assertEquals(result, book1);

//        List<BookEntity> bookEntity = new ArrayList<>();
//        BookEntity book3 = new BookEntity(1, "Jack Sparrow", "Test desc", true, "John");
//        BookEntity book4 = new BookEntity(2, "Peterpan", "Test desc2", true, "Peter");
//        bookEntity.add(book3);
//        bookEntity.add(book4);
//        lenient().when(bookRepository.findAll()).thenReturn(bookEntity);
//        BookEntity book2 = bookRepository.findById(1L);
//        assertEquals(book2, result);
//        verify(bookRepository).findById(1L).get();
    }

    @Test
    void addBook() {
        BookEntity bookToAdd = new BookEntity(1, "Jack Sparrow", "Test desc", true, "John");

        lenient().when(bookImp.addBook(any(BookEntity.class))).thenReturn(bookToAdd);
        BookEntity addedBook = bookController.addBook(bookToAdd);
        assertNotNull(addedBook);
        assertEquals(1, addedBook.getId());
        assertEquals("Jack Sparrow", addedBook.getTitle());
        assertEquals("John", addedBook.getWriter());
        assertEquals(true, addedBook.isPublished());
        assertEquals("Test desc", addedBook.getDescription());
        assertEquals("Book [id=1, title=Jack Sparrow, desc=Test desc, published=true]", addedBook.toString());
        verify(bookService, times(1)).addBook(bookToAdd);
    }

    @Test
    void updateBook() {
        BookEntity bookEntity = new BookEntity(1, "Jack Sparrow", "Test desc", true, "John");
        BookModel book1 = new BookModel(1, "Jack Sparrow", "Test desc", true, "John", null);
        BookModel updatedbook1 = new BookModel(1, "new Jack Sparrow", "new Test desc", false, "new John", null);
//        BookModel book2 = new BookModel(2, "Peterpan", "Test desc2", true, "Peter", null);
        lenient().when(bookController.updateBook(eq(1L), any(BookEntity.class))).thenReturn(updatedbook1);
        lenient().when(bookImp.updateBook(eq(1L), any(BookEntity.class))).thenReturn(updatedbook1);
        BookModel resultTemp = bookImp.updateBook(1L, bookEntity);
        BookModel result = bookController.updateBook(1L, bookEntity);
        assertEquals(result, updatedbook1);
        assertEquals(result, resultTemp);
        verify(bookImp, times(1)).updateBook(eq(1L), any(BookEntity.class));
    }

    @Test
    void deleteBook() {
//        List<BookEntity> bookEntities = new ArrayList<>();
//        BookEntity bookEntity = new BookEntity(1, "Jack Sparrow", "Test desc", true, "John");
//        bookEntities.add(bookEntity);
//        lenient().when(bookController.deleteBook(eq(1L))).thenReturn(bookEntity);
//        BookEntity resultTemp = bookImp.deleteBook(1L);
//        assertNull(bookEntities);
    }

}