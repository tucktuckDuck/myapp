package com.miniproject.bookshelf.controller;

import com.miniproject.bookshelf.controller.CatagoryController;
import com.miniproject.bookshelf.repository.CatagoryRepository;
import com.miniproject.bookshelf.service.CatagoryImp;
import com.miniproject.bookshelf.service.CatagoryService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;

class CatagoryControllerTest {
    @InjectMocks
    CatagoryController catagoryController;
    @Mock
    CatagoryService catagoryService;

    @Mock
    CatagoryImp catagoryImp;

    @Mock
    CatagoryRepository catagoryRepository;

    @Test
    void getAllCatagory() {
//        List<CatagoryModel> catagoryList = new ArrayList<>();
//        CatagoryModel catagory1 = new CatagoryModel(1, "Jack Sparrow", "Test desc", true, "John", null);
//        CatagoryModel catagory2 = new CatagoryModel(2, "Peterpan", "Test desc2", true, "Peter", null);
//        catagoryList.add(catagory1);
//        catagoryList.add(catagory2);
//        lenient().when(catagoryService.getAllCatagories()).thenReturn(catagoryList);
//        List<CatagoryModel> result = catagoryController.getAllCatagory();
//        assertEquals(result.size(), 2);
//        assertEquals(result, catagoryList);
//
//        List<CatagoryEntity> catagoryEntity = new ArrayList<>();
//        CatagoryEntity catagory3 = new CatagoryEntity(1, "Jack Sparrow", "Test desc", true, "John");
//        CatagoryEntity catagory4 = new CatagoryEntity(2, "Peterpan", "Test desc2", true, "Peter");
//        catagoryEntity.add(catagory3);
//        catagoryEntity.add(catagory4);
//        lenient().when(catagoryRepository.findAll()).thenReturn(catagoryEntity);
//        List<CatagoryEntity> catagorys = catagoryRepository.findAll();
////        System.out.println(catagorys);
//        assertEquals(catagoryEntity, catagorys);
//        verify(catagoryRepository).findAll();
    }

    @Test
    void getCatagoryById() {
    }

    @Test
    void addCatagory() {
    }

    @Test
    void updateCatagory() {
    }

    @Test
    void deleteCatagory() {
    }
}